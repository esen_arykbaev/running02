export const pushDATA = [
    {
        id: 1,
        img: 'https://source.unsplash.com/collection/4687444/random1',
        title: 'Еда и напитки',
        text: 'Ваш заказ забронирован',
        date: new Date().toJSON()
    },
    {
        id: 2,
        img: '',
        title: 'Пакет тренировок',
        text: 'Практикующий тренер по методу Пила...',
        date: new Date().toJSON()
    },
    {
        id: 3,
        img: 'https://source.unsplash.com/collection/4687444/random3',
        title: 'Статьи',
        text: 'Успейте посмотреть новую статью “Фор...',
        date: new Date().toJSON()
    }
];