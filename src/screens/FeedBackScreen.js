import React, {useState} from 'react';
import {View, StyleSheet, FlatList, Text, ImageBackground, Platform} from 'react-native';
import SegmentedControlTab from "react-native-segmented-control-tab";
import {DATA} from "../data";
import {pushDATA} from "../pushData";
import {Push} from "../components/Push";
import {Post} from "../components/Post";
import {THEME} from "../theme";

export const FeedBackScreen = () => {

    const [selectedIndex, setSelectedIndex] = useState(0);

    return (
        <ImageBackground style={styles.image} source={require('../../assets/bg1.png')} resizeMode="stretch">
            <View style={styles.container}>
                <View style={styles.tabContainer}>
                    <SegmentedControlTab
                        values={['Чаты', 'Уведомления']}
                        selectedIndex={selectedIndex}
                        onTabPress={index => setSelectedIndex(index)}
                        tabsContainerStyle={styles.tabsContainerStyle}
                        tabStyle={styles.tabStyle}
                        tabTextStyle={styles.tabTextStyle}
                        activeTabStyle={styles.activeTabStyle}
                        activeTabTextStyle={styles.activeTabTextStyle}
                    />
                </View>

                {selectedIndex === 0 ? (<FlatList
                    data={DATA}
                    keyExtractor={post => post.id.toString()}
                    renderItem={({item}) => <Post post={item} />}
                />) : (<FlatList
                    data={pushDATA}
                    keyExtractor={push => push.id.toString()}
                    renderItem={({item}) => <Push push={item} />}
                />)}
            </View>
        </ImageBackground>
    )
};

const styles = StyleSheet.create({
    image: {
        width: "100%",
        height: "100%"
    },
    container: {
        marginTop: Platform.OS === 'ios' ? 88 : 50,
    },
    tabContainer: {
        padding: 16
    },
    tabsContainerStyle:{
        height: 34
    },
    tabStyle: {
        borderColor: THEME.MAIN_COLOR,
    },
    tabTextStyle: {
        fontFamily: "PFDinTextCompPro-Regular",
        color: THEME.MAIN_COLOR,
        fontWeight: "normal",
        fontSize: 15,
        lineHeight: 20,
        letterSpacing: -0.24
    },
    activeTabStyle: {
        backgroundColor: THEME.MAIN_COLOR
    },
    activeTabTextStyle: {
        color: "#fff"
    }
});