import * as Font from 'expo-font';

export async function bootstrap () {
    await Font.loadAsync({
        'comp-pro-bold': require('../assets/fonts/PFDinTextCompPro-Bold.ttf'),
        'comp-pro-medium': require('../assets/fonts/PFDinTextCompPro-Medium.ttf'),
        'comp-pro-regular': require('../assets/fonts/PFDinTextCompPro-Regular.ttf'),
        'comp-pro-thin': require('../assets/fonts/PFDinTextCompPro-Thin.ttf')
    })
}