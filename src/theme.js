export const THEME = {
    GRAY_COLOR: "#DADADA",
    MAIN_COLOR:"#F46F22",
    TEXT_COLOR: "#151C26",
    DANGER_COLOR: "red",
    DEFAULT_COLOR: "#fff"
};