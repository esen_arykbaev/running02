export const DATA = [
    {
        id: 1,
        img: 'https://source.unsplash.com/collection/4687444/random1',
        firstName: 'Денис',
        LastName: "Сычёв",
        text: 'Владелец и основатель бренда',
        date: new Date().toJSON(),
        chat: true
    },
    {
        id: 2,
        img: '',
        firstName: 'Ксения',
        LastName: "Кулешова",
        text: 'Практикующий тренер по методу Пила...',
        date: new Date().toJSON(),
        chat: true
    },
    {
        id: 3,
        img: 'https://source.unsplash.com/collection/4687444/random3',
        firstName: 'Юрий',
        LastName: "Гавриш",
        text: 'Фитнес-директор сети PMP',
        date: new Date().toJSON(),
        chat: false
    },
    {
        id: 4,
        img: 'https://source.unsplash.com/collection/4687444/random4',
        firstName: 'Татьяна',
        LastName: "Юркина",
        text: 'Сертифицированный инструктор Pilate...',
        date: new Date().toJSON(),
        chat: false
    },
    {
        id: 5,
        img: 'https://source.unsplash.com/collection/4687444/random5',
        firstName: 'Мила',
        LastName: " Яковлева",
        text: 'CrossFit Level 1 Trainer Certificate',
        date: new Date().toJSON(),
        chat: false
    }

];