import React from 'react';
import { StyleSheet , View, Text, ImageBackground, TouchableOpacity, Platform} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/AntDesign';

import { MainScreen } from "../screens/MainScreen";
import PushScreen from "../screens/PushScreen";
import { FeedBackScreen } from "../screens/FeedBackScreen";
import { THEME } from "../theme";

const Tab = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const PushStack = createStackNavigator();
const FeedBackStack = createStackNavigator();


function CustomHeader({ title, navigation }) {
    //console.log('navigation', navigation)
    return(
        <View style={styles.image}>
            <View style={{ flex: 1}} />
            <View style={{ flex: 3, justifyContent: "center" }}>
                <Text style={styles.headerTitle}>{title}</Text>
            </View>
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <TouchableOpacity onPress={() => navigation.navigate('Push')} style={styles.create}>
                    <Icon size={28} name="plus" color={THEME.MAIN_COLOR}/>
                </TouchableOpacity>
            </View>
        </View>
    )
}

function PushHeader({title}) {
    return(
        <View style={styles.image}>
            <View style={{ flex: 1 }} />
            <View style={{ flex: 3, justifyContent: "center" }}>
                <Text style={styles.headerTitle}>{title}</Text>
            </View>
            <View style={{ flex: 1}} />
        </View>
    )
}

const HomeStackNavigator = ({navigation}) => {
    return(
        <>
            <CustomHeader title="Учетные записи" navigation={navigation}/>
            <HomeStack.Navigator screenOptions={{
                headerShown: false,
            }} >
                <HomeStack.Screen name="Main" component={MainScreen} />
            </HomeStack.Navigator>
        </>
    )
};

const PushStackNavigator = () => {
    return(
        <>
            <PushHeader title="Создать Push-уведомление"/>
            <PushStack.Navigator screenOptions={{
                headerShown: false,
            }} >
                <PushStack.Screen name="Push" component={PushScreen} />
            </PushStack.Navigator>
        </>
    )
};
const FeedBackStackNavigator = ({navigation}) => {
    return(
        <>
            <CustomHeader title="Обратная связь" navigation={navigation}/>
            <FeedBackStack.Navigator screenOptions={{
                headerShown: false,
            }} >
                <FeedBackStack.Screen name="FeedBack" component={FeedBackScreen} />
            </FeedBackStack.Navigator>
        </>
    )
};

function MainTabNavigator () {
    return (
        <Tab.Navigator
            screenOptions={({route}) => ({
                tabBarIcon: ({color, size}) => {
                    let iconName;
                    if(route.name === 'Main') {
                        iconName = 'adduser'
                    } else if(route.name === 'Push') {
                        iconName = 'mail'
                    } else if(route.name === 'FeedBack'){
                        iconName = 'message1'
                    }
                    return <Icon name={iconName} size={size} color={color} />
                }
            })}
            tabBarOptions={{
                activeTintColor: THEME.MAIN_COLOR,
                labelStyle: {
                    fontFamily: 'PFDinTextCompPro-Medium',
                    fontSize: 13,
                    letterSpacing: 0.16
                },
                tabStyle: {
                    paddingTop: 7
                }
            }}
        >
            <Tab.Screen name="Main" component={HomeStackNavigator} options={{ title: "Учетные записи" }} />
            <Tab.Screen name="Push" component={PushStackNavigator} options={{ title: "Push-Уведомления" }} />
            <Tab.Screen name="FeedBack" component={FeedBackStackNavigator} options={{ title: "Обратная связь" }}   />
        </Tab.Navigator>
    );
}

export default function AppNavigation() {
    return (
        <NavigationContainer>
            <MainTabNavigator/>
        </NavigationContainer>
    )
};

export const styles = StyleSheet.create({
    headerTitle: {
        textAlign: "center",
        fontFamily: "PFDinTextCompPro-Medium",
        fontSize: 18,
        lineHeight: 22,
        letterSpacing: -0.41,
        color: THEME.TEXT_COLOR
    },
    tabNavigator: {
        paddingTop: 10
    },
    image: {
        width: "100%",
        height: Platform.OS === 'ios' ? 88 : 50,
        paddingTop: Platform.OS === 'ios' ? 50 : 30,
        flexDirection: "row",
        backgroundColor: "transparent",
        position: "absolute",
        top: 0,
        left: 0,
        zIndex: 2
    }
});