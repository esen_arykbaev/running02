import React from 'react';
import { HeaderButton } from 'react-navigation-header-buttons';
import { THEME } from "../theme";


export const AppHeaderIcon = props => (
    <HeaderButton
        {...props}
        iconSize={24}
        IconComponent={ Entypo }
        color={ THEME.MAIN_COLOR }
    />
);